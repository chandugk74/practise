package easycoding;

import java.util.Scanner;

public class IfElseEx {

	public static void main(String[] args) {
		int passingmarks, obtainedmarks;
		passingmarks = 40;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the obtained marks");
		obtainedmarks = sc.nextInt();
		if (obtainedmarks >= passingmarks) {
			System.out.println("you are passed");
		} else {
			System.out.println("You are failed");
		}
	}
}
