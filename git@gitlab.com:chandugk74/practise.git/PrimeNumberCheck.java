package easycoding;

import java.util.Scanner;

public class PrimeNumberCheck {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please the enter the number to check prime or  not");
		int n=sc.nextInt();
//		Flag variable is used as a signal in programming to let the 
//		program know that a certain condition has met. It usually acts
//		as a boolean variable indicating a condition to be either true or false.
		boolean flag=false;
		for(int i =2; i<=n/2; i++) {
			if(n%i==0) {
				flag=true;
				}
		}
		if (! flag) 
			System.out.println("Prime");
		else {
			System.out.println("Not Prime");
		}

	
}
}
