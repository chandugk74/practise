package easycoding;

public class staticInstanceMethod {
	public static void main(String[] args) {
	    display();  //calling without object
	    staticInstanceMethod t = new staticInstanceMethod();
	    t.show();  //calling using object
	  }

	  static void display() {
	    System.out.println("Programming is amazing.");
	  }
	 
	  void show(){
	    System.out.println("Java is awesome.");
	  }
}
